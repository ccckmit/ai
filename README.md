# 陳鍾誠的『人工智慧』課程 

* 程式碼: https://gitlab.com/ccckmit/ai2/
* 教科書: [人工智慧 -- 使用 Python 實作](https://misavo.com/blog/%E9%99%B3%E9%8D%BE%E8%AA%A0/%E6%9B%B8%E7%B1%8D/%E4%BA%BA%E5%B7%A5%E6%99%BA%E6%85%A7)


## 參考

* [李宏毅](https://speech.ee.ntu.edu.tw/~tlkagk/)
    *  [Deep Learning for Human Language Processing (2020,Spring)](http://speech.ee.ntu.edu.tw/~tlkagk/courses_DLHLP20.html)
    * [Machine Learning and having it deep and structured (2018,Spring)](http://speech.ee.ntu.edu.tw/~tlkagk/courses_MLDS18.html)
    * [李宏毅2020机器学习深度学习(完整版)国语](https://www.bilibili.com/video/av94519857/)

