# 用 Python 學數學

* [3Blue1Brown](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw)
    * [Multivariable calculus](https://www.khanacademy.org/math/multivariable-calculus)

## 參考文獻

* [用Python學數學(PDF)](http://www.goodbooks.com.tw/python/CH07.pdf)
* [數學與Python有機結合及統計學、微積分、線性代數相關資源、圖形軟體](https://www.jishuwen.com/d/27NM/zh-tw)
* [](https://docs.sympy.org/latest/modules/plotting.html)
* http://www.goodmath.org/blog/
* [丘成桐：数学史大纲（Brief History of Math）](https://mp.weixin.qq.com/s/kN5Bb19ZlOrLL8H5zf7zlQ)

## 綱領

* [朗蘭茲綱領](https://zh.wikipedia.org/wiki/%E6%9C%97%E8%98%AD%E8%8C%B2%E7%B6%B1%E9%A0%98)
    * [數學中 『神奇』 的大統一理論 一 朗蘭茲綱領 邵紅能](https://web.math.sinica.edu.tw/math_media/d422/42207.pdf)
* [愛爾蘭根綱領](https://zh.wikipedia.org/zh-hant/%E7%88%B1%E5%B0%94%E5%85%B0%E6%A0%B9%E7%BA%B2%E9%A2%86)
