# 數學

## 數學傳播季刊

* [用函數來思考(上)](http://web.math.sinica.edu.tw/math_media/d433/43304.pdf)
* [用函數來思考 (下)](https://web.math.sinica.edu.tw/math_media/d434/43404.pdf)
* [熔伽利略與勞倫茲變換於一爐 張海潮](https://web.math.sinica.edu.tw/math_media/d434/43402.pdf)
* [愛因斯坦的曲率公式和光經過太陽的偏折角度 張海潮](https://web.math.sinica.edu.tw/math_media/d422/42202.pdf)
* [電腦與數學 : 問題與展望 演講者 : Ronald Graham](https://web.math.sinica.edu.tw/math_media/d422/42201.pdf)
* [數學中 『神奇』 的大統一理論 一 朗蘭茲綱領 邵紅能](https://web.math.sinica.edu.tw/math_media/d422/42207.pdf)
    * 在數學中, 被稱為 『綱領』 的成果屈指可數, 出名的僅有愛爾蘭根 (Erlanger) 綱領、 希爾伯特 (Hilbert) 綱領和朗蘭茲綱領這三個。 
* [對廣義相對論三個預測的補充說明 一一附 1919/11/7 倫敦泰唔士報報導 張海潮](https://web.math.sinica.edu.tw/math_media/d424/42403.pdf)
* [積分因子 一 Lie 群之觀點 林琦焜](https://web.math.sinica.edu.tw/math_media/d412/41203.pdf)
